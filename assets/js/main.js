$(document).ready(function(){


  var client = ZAFClient.init();

  client.on('app.registered', function() {
  client.invoke('resize', { width: '400px', height: '400px' });
  ViewData(client);
});

Footer();
ocultarCampos();

$("#inci").click(function(){
  mostrarCampos();
});
$("#like").click(function(){
  ocultarCampos();
});
$("#no").click(function(){
  ocultarCampos();
});
});

function Footer(){
  html = '<footer>\
            <hr>\
              <button class="c-btn c-btn--icon c-btn--pill c-btn--basic c-btn--muted">\
                <svg class="c-btn__icon">\
                  <svg viewBox="0 0 26 26" id="zd-svg-icon-26-zendesk" width="100%" height="100%"><path fill="currentColor" d="M12 8.2v14.5H0zM12 3c0 3.3-2.7 6-6 6S0 6.3 0 3h12zm2 19.7c0-3.3 2.7-6 6-6s6 2.7 6 6H14zm0-5.2V3h12z"></path></svg>\
                </svg>\
              </button>\
              Desarrollado por <a href="http://www.zerviz.cl" target="_blank" >ZerviZ</a> 2018\
          </footer>';

  $("#footer").html(html);  
}

function ocultarCampos(){
  $("#datos1").hide();
  $("#datos2").hide();
  $("#datos3").hide();
  $("#datos4").hide();
  $("#datos5").hide();
  $("#datos6").hide();
}
function mostrarCampos(){
  $("#datos1").show();
  $("#datos2").show();
  $("#datos3").show();
  $("#datos4").show();
  $("#datos5").show();
  $("#datos6").show();
}
$("#btnLimpiar").click(function(){
  $("#datos").val("");
  $("#datos1").val("");
  $("#datos2").val("");
  $("#datos3").val("");
  $("#datos4").val("");
  $("#datos5").val("");
  $("#datos6").val("");
  $("#t0").empty();
  $("#t1").empty();
  $("#t2").empty();
  $("#t3").empty();
  $("#t4").empty();
  $("#t5").empty();
  $("#t6").empty();
  $("#t7").empty();
  $("#t8").empty();
  $("#t9").empty();
  $("#t10").empty();
  $("#t11").empty();
  $("#t12").empty();
});
 

function ViewData(client){
   $( "#ConsultaGuiSI" ).unbind( "click" ).click(function (){
    if ($("#like").is(":checked")==false && $("#no").is(":checked")==false && $("#inci").is(":checked")==false) {
       $("#ta").html('<p class="text-danger"> Debe seleccionar un tipo de servicio</p></br>');
    }else{
    $("#tablaWs2").remove();
    $("#tablaWs3").remove();
    if($("#datos").val()==''){
      $("#ta").html('<p class="text-danger"> Debe ingresar un valor</p>');
    }else{
   
      //$("#like").change(function(){
        //if (this.checked == true){
     //9272-10    (1)
     //602380800
    // if ($("#like")[1].checked==true){
    if ($("#like").is(":checked")==true){
      var checkBox = document.getElementById("ConsultaGuiSI");
      var fetchItem = {
        url: 'https://wsintegracion.holascharff.com/Servicios/b2bScharffZen/Consulta/GuiaSN',
        cors: false,
        type: 'POST',
        contentType: 'application/json',
        data :  JSON.stringify({nroGuia : $('#datos').val()})
      };

      client.request(fetchItem).then(function(result) {
         
        if(result["length"]==0){
          $("#ta").html('<p class="text-danger">No se encontró resultado</p> </br>');
        }else{

 $("#ta").html('<table class="tablaws" id="tablaWs1">  <tr>    <td id="iz">Empresa</td>    <td id = "t0"></td>  </tr>  <tr>    <td id="iz">Sucursal</td>    <td id = "t1"></td>  </tr>  <tr>  <td id="iz">Unidad de negocio</td><td id = "t2"></td></tr>  <tr>    <td id="iz">Producto</td> <td id = "t3"></td> </tr>  <tr><td  id="iz">Año interno</td><td id = "t4"></td></tr>  <tr><td id="iz">Número interno</td>    <td id = "t5"></td>  </tr>  <tr>    <td id="iz">Número de tracking</td>    <td id = "t6"></td>  </tr>  <tr>    <td id="iz">Año manifiesto</td>    <td id = "t7"></td>  </tr>  <tr>    <td id="iz">Número manifiesto</td>    <td id = "t8"></td>  </tr> </table></br>');
        var array = [];
        for (var a in result){
          for (var i in result[a]){
            array.push(result[a][i]);
          }
          for(x = 0; x < 9; x++){
            var textnode = document.createTextNode(array[x]);
            document.getElementById("t"+x).appendChild(textnode);
          } 
        } 
      }
      });
    } else if ($("#no").is(":checked")==true){
     $("#tablaWs1").remove();
     $("#tablaWs3").remove();
      var checkBox = document.getElementById("ConsultaGuiSI");
      var fetchItem = {
        url: 'https://wsintegracion.holascharff.com/Servicios/b2bScharffZen/Consulta/GuiaSI',
        cors: false,
        type: 'POST',
        contentType: 'application/json',
        data :  JSON.stringify({nroGuia : $('#datos').val()})
      };

      client.request(fetchItem).then(function(result) {
        if(result["length"]==0){
          $("#ta").html('<p class="text-danger">No se encontró resultado</p> </br>');
        }else{
          $("#ta").html('<table class="tablaws" id="tablaWs2">  <tr>    <td id="iz">Empresa</td>    <td id = "t0"></td>  </tr>  <tr>    <td id="iz">Sucursal</td>    <td id = "t1"></td>  </tr>  <tr>  <td id="iz">Unidad de negocio</td><td id = "t2"></td></tr>  <tr>    <td id="iz">Producto</td> <td id = "t3"></td> </tr>  <tr><td id="iz">Año interno</td><td id = "t4"></td></tr>  <tr><td id="iz">Número interno</td>    <td id = "t5"></td>  </tr>  <tr>    <td id="iz">Número de tracking</td>    <td id = "t6"></td>  </tr>  <tr>    <td id="iz">Año manifiesto</td>    <td id = "t7"></td>  </tr>  <tr>    <td id="iz">Número manifiesto</td>    <td id = "t8"></td>  </tr> </table></br>');
        var array = [];
        for (var a in result){
          for (var i in result[a]){
            array.push(result[a][i]);
          }
          for(x = 0; x < 9; x++){
            var textnode = document.createTextNode(array[x]);
            document.getElementById("t"+x).appendChild(textnode);
          } 
        } 
      }
      });

    } else if  ($("#inci").is(":checked")==true){
      
      $("#tablaWs1").remove();
      $("#tablaWs2").remove();
      mostrarCampos();  
      if($("#datos").val()==''||$("#datos1").val()=='' || $("#datos2").val()=='' || $("#datos3").val()=='' ||
        $("#datos4").val()=='' || $("#datos5").val()=='' || $("#datos6").val()==''){
      $("#ta").html('<p class="text-danger" id="pf">Los campos no deben estar vacíos</p> </br>');
    }else{
      $("#pf").remove();
      var checkBox = document.getElementById("ConsultaGuiSI");
      var fetchItem = {
        url: 'https://wsintegracion.holascharff.com/Servicios/b2bScharffZen/Consulta/incidencias',
        cors: false,
        type: 'POST',
        contentType: 'application/json',
        data :  JSON.stringify({empr_cod : $('#datos1').val(),suc_cod : $('#datos2').val(),
        uni_neg_cod : $('#datos3').val(),pro_cod : $('#datos4').val(),ord_ano : $('#datos5').val(),
        ord_nro : $('#datos6').val(),ord_det_nro : $('#datos').val()})
      };

      client.request(fetchItem).then(function(result) {
        if(result["length"]==0){
          $("#ta").html('<p class="text-danger" id="pp">No se encontró resultado</p> </br>');
        }else{
          $("#pp").remove();
        $("#inc").html('<table class="tablaws" id="tablaWs3"> <tr> <td id="iz">GuiaDetalleNro</td><td id="t0"> </td> </tr> <tr> <td id="iz">Numero</td> <td id ="t1"></td> </tr> <tr> <td id="iz">RegOrden</td><td id = "t2"></td></tr>  <tr>    <td id="iz">Fecha</td> <td id = "t3"></td> </tr>  <tr><td id="iz">Hora</td><td id = "t4"></td></tr>  <tr><td id="iz">CodigoOfi</td>    <td id = "t5"></td>  </tr>  <tr> <td id="iz">CodigoDes</td>    <td id = "t6"></td>  </tr>  <tr>    <td id="iz">Descripcion</td>    <td id = "t7"></td> </tr> <tr> <td id="iz">EmpGenAbr</td> <td id = "t8"></td>  </tr> <tr> <td id="iz">TipoOfi</td> <td id="t9"></td> </tr><tr> <td id="iz">TipoDes</td> <td id = "t10"></td> </tr> <tr> <td id="iz">Ambito</td> <td id ="t11"></td>  </tr><tr> <td id="iz">Motivo</td> <td id="t12"></td> </tr> </table> </br>');
        var array = [];
        for (var a in result){
          for (var i in result[a]){
            array.push(result[a][i]);
          }
          for(x = 0; x < 9; x++){
            var textnode = document.createTextNode(array[x]);
            document.getElementById("t"+x).appendChild(textnode);
          } 
        } 
      }
       });
    }
  }
}
}
  });//el fin del function(client)
}
